from __future__ import annotations
import cv2
import mss
import numpy
import numpy as np
from typing import Tuple, List, Callable, Coroutine
import asyncio
import logging

from numpy import ndarray


logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)s %(message)s',
    datefmt='%Y-%m-%d | %H:%M:%S'
)


class Rectangle:

    def __init__(
            self,
            x: int,
            y: int,
            width: int,
            height: int,
    ):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    @property
    def middle_point(self) -> Tuple[int, int]:
        return self.x + self.width // 2, self.y + self.height // 2

    @property
    def bottom_right(self) -> Tuple[int, int]:
        return self.x + self.width, self.y + self.height

    @property
    def top_left(self) -> Tuple[int, int]:
        return self.x, self.y

    def enclose(
            self,
            image,
            color: Tuple[int, int, int] = (0, 255, 255),
            thickness: int = 2
    ) -> None:
        cv2.rectangle(image, self.top_left, self.bottom_right, color, thickness)

    def draw_center(
            self,
            image,
            color: Tuple[int, int, int] = (0, 255, 255),
            boldest: int = 5,
            thickness: int = 2,
    ) -> None:
        x, y = self.middle_point
        cv2.rectangle(image, self.middle_point, (x + boldest, y + boldest), color, thickness)

    def as_tuple(self) -> Tuple[int, int, int, int]:
        return self.x, self.y, self.width, self.height

    def __repr__(self):
        return f"< Rectangle x = {self.x} | y = {self.y}>"


class TemplateDetection:

    def __init__(
            self,
            source_image,
            rectangles: List[Rectangle],
            width,
            height,
            template_action: Callable[[Rectangle], None] = None,
    ):
        self.source_image = source_image
        self.width = width
        self.height = height
        self.template_action = template_action
        self.rectangles = rectangles

    async def perform(self) -> Coroutine:
        return asyncio.gather(*[
                asyncio.create_task(self.template_action(rectangle))
                for rectangle in self.rectangles
            ])

    @classmethod
    def from_locations(
            cls,
            source_image: ndarray,
            locations: ndarray,
            width: int,
            height: int,
            template_action: Callable = None
    ) -> TemplateDetection:
        rectangles = []

        for index, point in enumerate(zip(*locations[::-1])):
            rectangle = Rectangle(
                x=point[0], y=point[1],
                width=width, height=height
            )
            rectangles.append(rectangle)

        return TemplateDetection(
            source_image=source_image,
            rectangles=rectangles,
            width=width,
            height=height,
            template_action=template_action
        )

    @classmethod
    def from_rectangles_tuples(
            cls,
            source_image: ndarray,
            rectangles: List[tuple],
            width: int,
            height: int,
            template_action: Callable = None
    ) -> TemplateDetection:
        rectangles_ = [Rectangle(*rect_tuple) for rect_tuple in rectangles]

        return TemplateDetection(
            source_image=source_image,
            rectangles=rectangles_,
            width=width,
            height=height,
            template_action=template_action
        )

    @property
    def middle_points(self) -> Tuple[int, int]:
        return [rect.middle_point for rect in self.rectangles]

    def clear_same(self, group_with: int = 1, closest: float = 0.2) -> TemplateDetection:
        rectangles = []

        for rect in self.rectangles:
            rectangles.append(rect.as_tuple())
            rectangles.append(rect.as_tuple())

        rectangles, weight = cv2.groupRectangles(rectangles, group_with, closest)

        return self.from_rectangles_tuples(
            source_image=self.source_image,
            rectangles=rectangles,
            width=self.width,
            height=self.height,
            template_action=self.template_action
        )


class Template:

    def __init__(
            self,
            image_path: str,
            threshold: float = 0.8,
            tm: int = cv2.TM_CCOEFF_NORMED
    ):
        self.image_path = image_path
        self.image = cv2.imread(image_path, 0)
        self.threshold = threshold
        self.tm = tm

    def detect_template(self, source_image):
        res = cv2.matchTemplate(
            cv2.cvtColor(source_image, cv2.COLOR_BGR2GRAY), self.image, self.tm
        )
        threshold = self.threshold
        locations = np.where(res >= threshold)
        width, height = self.image.shape[::-1]

        return TemplateDetection.from_locations(
            source_image=self.image,
            locations=locations,
            width=width,
            height=height,
            template_action=self.perform_action,
        )

    async def perform_action(self, rectangle: Rectangle) -> None:
        ...


def create_screenshot(rectangle):
    scr = mss.mss()
    arr = numpy.array(scr.grab({
        'top': rectangle.top,
        'left': rectangle.left,
        'width': rectangle.width(),
        'height': rectangle.height()
    }))
    logging.info('Screenshot taken')
    return arr


class Observer:

    def __init__(
            self,
            window,
            pixel_rejection: int = 8
    ):
        self.window = window
        self.pixel_rejection = pixel_rejection
        self.templates: List[Template] = []

    def add_template(self, template: Template) -> None:
        self.templates.append(template)

    async def update(self) -> None:
        for template in self.templates:
            screen = create_screenshot()

            detection = template.\
                detect_template(screen).\
                clear_same(pixel_rejection=self.pixel_rejection)

            await detection.perform()

    async def observe(self, timeout_seconds: int = 1) -> None:
        while True:
            await asyncio.sleep(timeout_seconds)
            await self.update()
