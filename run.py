import asyncio
import sys
from pathlib import Path

import pyautogui

from template import Template, Observer, Rectangle
from pywinauto import Desktop


WINDOW_NAME = 'Game window name'  # I've used 'gameloop', because I run this game in emulator
desktop = Desktop()


try:
    main_window = list(filter(
        lambda window: WINDOW_NAME in window.window_text().lower(),
        desktop.windows()
    ))[0]
except IndexError:
    print('Cant find window')
    sys.exit(0)


class ClickTemplate(Template):

    async def perform_action(self, rectangle: Rectangle) -> None:
        x, y = rectangle.middle_point
        pyautogui.moveTo(x, y+20)
        pyautogui.mouseDown()
        pyautogui.click()
        pyautogui.mouseUp()


root = Path(__file__).root
templates_path = root / Path('templates')


def template(template_location: str) -> str:
    return str(templates_path / Path(template_location))


templates = [
    ClickTemplate(template('template_money.png')),
    ClickTemplate(template('template_food.png')),
    ClickTemplate(template('template_snowcoin.png')),
    ClickTemplate(template('template_snowcoin_1.png')),
    ClickTemplate(template('template_snowcoin_2.png'), threshold=0.7),
    ClickTemplate(template('template_snowcoin_3.png'), threshold=0.7),
    ClickTemplate(template('template_scretch.png')),
    ClickTemplate(template('template_cancel.png')),
    ClickTemplate(template('template_cancel_big.png')),
    ClickTemplate(template('template_continue.png')),
    ClickTemplate(template('template_lets.png'))
]


observer = Observer(window=main_window)
observer.templates.extend(templates)


async def main() -> None:
    await observer.observe(timeout_seconds=0.2)


if __name__ == '__main__':
    asyncio.run(main())
